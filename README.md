# MiniscopedBehavior

User-friendly tools for analyzing miniscope calcium imaging and tracking labels on videos.

## CaImAn_GetCellsMiniscope.ipynb
Jupyter notebook for extracting cells fluorescence signal using CaImAn.

## DeepLabCut_TrackLabels.ipynb
Jupyter notebook for tracking labels defined on a previously trained network using DeepLabCut.

## DeepLabCut_TrainNetwork.ipynb
Jupyter notebook for training a deeplabcut network using DeepLabCut.

## Mice-Fingers_Forepaws_Nose_Pellet
Trained network for tracking a single mouse reaching for a sugar pellet with its left paw. Videos were recorded at front view. Parts tracked are: right paw, left paw, the first 3 fingers of each paw and nose; and sugar pellets of different colors. The network was trained such that wherever the likelihood of the left paw and the 3 left fingers is >0.7 the animal is reaching for the pellet with its left hand. This allows for reliably detecting reach events.
